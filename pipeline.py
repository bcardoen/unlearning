from sklearn.model_selection import KFold
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error
from scipy.stats import pearsonr
from copy import deepcopy
import matplotlib.pyplot as plt



class Loss:
    '''
    A Loss object, callable loss function wrapper.
    '''
    def __init__(self, loss_function):
        self.loss_function = loss_function

    def __call__(self, y_predict, y_test):
        return self.loss_function(y_predict, y_test)


class MSELoss(Loss):
    def __init__(self):
        super().__init__(mean_squared_error)


class MAELoss(Loss):
    def __init__(self):
        super().__init__(mean_absolute_error)


class Trainer:
    def __init__(self, k, loss_object=MSELoss()):
        self.k = k
        assert(isinstance(loss_object, Loss))
        self.loss_object = loss_object
        self.parameter_space = {}
        self.record = {}
        self.model_type = None

    def __call__(self, X_train, y_train):
        self.best_p, self.best_loss = self._construct(X_train, y_train)
        model = self.model_type(X_train, y_train, self.best_p)
        return model

    def _construct(self, X, y):
        # print("Constructing model in Trainer")
        assert(isinstance(X, np.ndarray))
        assert(isinstance(y, np.ndarray))
        assert(self.model_type and "Set the model_type to your own class")
        self.record = {}
        record = self.record
        for parameter_set in self.parameter_space:

            # print("Testing parameter set")
            record[parameter_set] = {}
            kf = KFold(n_splits=self.k)
            loss_sum = 0

            for train_index, test_index in kf.split(X):
                X_train, X_test = X[train_index], X[test_index]
                y_train, y_test = y[train_index], y[test_index]

                model = self.model_type(X_train, y_train, parameter_set)     # make logistic regression with parameter lambda = 0.1, X = [123] y
                y_predict = model(X_test)                                   # model.predict([4])
                loss_sum += self.loss_object(y_predict, y_test)             # sum loss for this model, parameter, kfold
            record[parameter_set] = loss_sum / self.k                       # store mean loss for this model and parameter  , lambda = 0.1, meanloss = 0.00001

        best_loss, best_p = float('inf'), {}
        for parameter_set, mean_loss in record.items():
            if mean_loss <= best_loss:
                best_loss = mean_loss
                best_p = parameter_set
        # print("Best parameter_set is {} for min loss {}".format(best_p, best_loss))
        return (best_p, best_loss)                                          # 0.1 , 0.000001


class Model:
    def __init__(self, X_train, y_train, parameter_set):
        self.info = None
        self.X_train = X_train
        self.y_train = y_train
        self.parameter_set = parameter_set
        self._train()


    def __call__(self, X_test):
        assert(False and "Override this method with your own!!")
        y_predict = None
        return y_predict

    def _train(self):
        print("Superclass stubbed training function doing not much...")

    @property
    def model_info(self):
        return self.info


class Pipeline:
    '''
    A pipeline bundles a set of trainers (regressors)
    '''
    def __init__(self, trainers, k, X, y, loss_object=MSELoss()):
        '''

        :param trainers: Set of regressors
        :param k: K (fold)
        :param loss_object: @see MSELoss
        :param X: Data Matrix (rows = samples, columns = features)
        :param y: Class Vector (rows = samples)
        '''
        self.trainers = trainers
        self.k = k
        self.X = X
        self.y = y
        assert(isinstance(loss_object, Loss))
        self.loss_object = loss_object
        self.record = {}


    def __call__(self):
        print("Executing pipeline")
        self.record['mean_loss'] = {}
        self.record['mean_r'] = {}
        self.record['trainer_model_info'] = {}
        X, y = self.X, self.y

        for trainer_index, trainer in enumerate(self.trainers):
            print("Executing pipeline for trainer {}".format(trainer.__class__))
            self.record[trainer_index] = {} # trainer e.g. Logistic regression with it's known best parameter l = 0.1
            kf = KFold(n_splits=self.k)     # Comparing with other regressors

            minloss = float('inf')
            minmodel = None
            for i, (train_index, test_index) in enumerate(kf.split(X)):
                X_train, X_test = X[train_index], X[test_index]
                y_train, y_test = y[train_index], y[test_index]
                model = trainer(X_train, y_train)
                y_predict = model(X_test)
                loss = self.loss_object(y_predict, y_test)
                if loss < minloss:
                    minloss = loss
                    minmodel  = deepcopy(y_predict), deepcopy(y_test)
                    try:
                        print("Params for best are {}".format(model._est.get_params(deep=True)))
                    except:
                        pass
                r = pearsonr(y_predict, y_test.ravel())[0]
                self.record[trainer_index][i] = loss, r

            yp, yt = minmodel
            fig, ax = plt.subplots()
            ax.scatter(yt, yp, edgecolors=(0, 0, 0))
            ax.plot([min(yt), max(yt)], [min(yt), max(yt)], 'k--', lw=4)
            ax.set_xlabel('Measured')
            ax.set_ylabel('Predicted')
            plt.savefig("Regressor {} .png".format(trainer.__class__))

            self.record['mean_loss'][trainer_index] = sum([loss for (loss, _) in self.record[trainer_index].values()])/self.k
            self.record['mean_r'][trainer_index] = sum([r for (_, r) in self.record[trainer_index].values()]) / self.k
            print("Results for  {} :: AVG MSE k=5 {} avg R k=5 {}".format(trainer.__class__, self.record['mean_loss'][trainer_index], self.record['mean_r'][trainer_index]))
            model = trainer(self.X, self.y)
            self.record['trainer_model_info'][trainer_index] = model.info

        # Now we're done, look up whichever trainer had the lowest mean loss
        best_mean_loss, best_trainer_index = min( (mean_loss, trainer_index) for  trainer_index, mean_loss in self.record['mean_loss'].items() )
        print("Best trainer is {} with info {} and mean loss {}".format(self.trainers[best_trainer_index], self.record['trainer_model_info'][best_trainer_index], best_mean_loss))





if __name__=="__main__":
    print("Don't call me")
