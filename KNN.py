
# coding: utf-8

# In[68]:


import sys
import scipy
import numpy as np
import pandas as pd
from time import time
from datafilter import loadData
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import accuracy_score
from pipeline import Trainer, Model, MSELoss
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_squared_error, mean_absolute_error


# In[60]:


class KNNModel(Model):
    def __init__(self, X_train, y_train, parameter_set):
        y_train = y_train.ravel()
        super().__init__(X_train, y_train, parameter_set)
    
    def _train(self):
        self._est = KNeighborsRegressor(n_neighbors=70,weights='uniform',algorithm='auto').fit(self.X_train, self.y_train)


        #set info here
    def __call__(self, X_test):
        y_predict = self._est.predict(X_test)
        return y_predict

class KNNTrainer(Trainer):
    def __init__(self, k, loss_object=MSELoss()):
        super().__init__(k, loss_object=loss_object)
        self.model_type =KNNModel
        self.parameter_space[0] = {'n_neighbors': 70, 'weights': 'uniform'}


# In[61]:


def report(results, n_top=1):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")


# In[71]:


def ptype():
    data = loadData(pca=True)
    X_train = data[0]
    X_test = data[2]
    y_test = data[3].ravel()
    y_train = data[1].ravel()
    
    #Grid Search to find best parameter values
    param_grid = [{'weights': ['uniform', 'distance'], 'n_neighbors': [5, 10, 20, 30, 40, 50, 60, 70, 80]}]
    clf = KNeighborsRegressor()
    grid_search = GridSearchCV(clf, param_grid, cv=5)
    grid_search_start = time()
    grid_search.fit(X_train,y_train)
    print("GridSearchCV took %.2f seconds" % (time() - grid_search_start))
    
    #To retrieve the top n results as specified in n_top parameter of report()
    report(grid_search.cv_results_)
    
    #Using the result from Grid Search (n_neighbors: 80, weights: "uniform")    
    est = KNeighborsRegressor(n_neighbors=80,weights='uniform').fit(X_train,y_train)
    y_predict = est.predict(X_test)
    print("R-Squared error {}".format(est.score(X_test,y_test)))
    mse = mean_squared_error(y_test, y_predict)
    print("MSE {:.2E}".format(mse))
    
    
    #Plot
    fig, ax = plt.subplots()
    ax.scatter(y_test, y_predict, edgecolors=(0, 0, 0))
    ax.plot([y_test.min(), y_test.max()], [y_test.min(), y_test.max()])
    ax.set_xlabel('Measured')
    ax.set_ylabel('Predicted')
    plt.show()
   
    


# In[72]:


if __name__=="__main__":
    ptype()

