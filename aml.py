# -*- encoding: utf-8 -*-
import sklearn.model_selection
import sklearn.datasets
import sklearn.metrics
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
import numpy as np
from datafilter import loadData

import autosklearn.regression


def main():
    data = loadData(pca=True)
    _, _, _, _, Xt, yt = loadData(pca=True)

    yt = yt.ravel()

    # feature_types = (['numerical'] * 3) + ['categorical'] + (['numerical'] * 9)
    feature_types = (['numerical'] * 163)
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(Xt, yt, random_state=1)

    automl = autosklearn.regression.AutoSklearnRegressor(
        time_left_for_this_task=3600, per_run_time_limit=60,
        tmp_folder='/local-scratch/autoslearn_regression_example_tmp',
        output_folder='/local-scratch/autosklearn_regression_example_out',
        include_preprocessors=["no_preprocessing"])
    automl.fit(X_train, y_train, dataset_name='brain',
               feat_type=feature_types)

    print(automl.show_models())
    predictions = automl.predict(X_test)
    print("R2 score:", sklearn.metrics.r2_score(y_test, predictions))
    r = pearsonr(y_test, predictions.ravel())
    print("Pearson R {} ".format(r))
    print("MSE score:", sklearn.metrics.mean_squared_error(y_test, predictions))
    print("MAE score:", sklearn.metrics.mean_absolute_error(y_test, predictions))
    fig, ax = plt.subplots()
    ax.scatter(y_test, predictions, edgecolors=(0, 0, 0))
    ax.plot([min(y_test), max(y_test)], [min(predictions), max(predictions)], 'k--', lw=4)
    ax.set_xlabel('Measured')
    ax.set_ylabel('Predicted')
    plt.savefig("Regressor {} .png".format("Automl"))


if __name__ == '__main__':
    main()
