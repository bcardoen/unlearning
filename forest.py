import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score

from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import GridSearchCV
from time import time

from sklearn import ensemble
from sklearn import datasets
from sklearn.utils import shuffle
from datafilter import loadData
import sys
import matplotlib as ml
from pipeline import Trainer, Model, MSELoss
ml.use('AGG')
import matplotlib.pyplot as plt

# Mean of Y 422.9631901840491 and Var 121528.61214196998


class FModel(Model):
    def __init__(self, X_train, y_train, parameter_set):
        y_train = y_train.ravel()
        super().__init__(X_train, y_train, parameter_set)

    def _train(self):
        self._est = RandomForestRegressor(criterion='mse', n_estimators=81).fit(self.X_train, self.y_train)
        # features = self._est.feature_importances_
        # self.info = {'name':"GradientBoostingRegressor" , 'features': sorted( ( (f,index) for index, f in enumerate(features) if f > sys.float_info.epsilon ) , reverse=True)}
        self.info = {'name':"RForest tuned by GSearch"}

    def __call__(self, X_test):
        y_predict = self._est.predict(X_test)
        return y_predict



class FTrainer(Trainer):
    def __init__(self, k, loss_object=MSELoss()):
        super().__init__(k, loss_object=loss_object)
        self.model_type = FModel
        self.parameter_space[0] = {'n_estimators':81}

def report(results, n_top=1):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")

def ptype():
    data = loadData(pca=True)
    X_train = data[0]
    X_test = data[2]
    y_test = data[3].ravel()
    y_train = data[1].ravel()

    est = RandomForestRegressor(criterion='mse')
    param_grid = [{'n_estimators': [i for i in range(1,100, 10)]}]
    grid_search = GridSearchCV(est, param_grid, cv=5)
    grid_search_start = time()
    grid_search.fit(X_train, y_train)
    print("GridSearchCV took %.2f seconds" % (time() - grid_search_start))

    # To retrieve the top n results as specified in n_top parameter of report()
    report(grid_search.cv_results_)
    est.fit(X_train,y_train)
    print(mean_squared_error(y_test, est.predict(X_test)))
    y_predict = est.predict(X_test)
    mse, mae, r2 = mean_squared_error(y_test, y_predict), mean_absolute_error(y_test, y_predict), r2_score(y_test, y_predict)
    print("MSE {:.2E} MAE {:.2E} R2 {:.2E}".format(mse, mae, r2))

    # features = est.feature_importances_
    tol = sys.float_info.epsilon
    # important = sorted( ( (f,index) for index, f in enumerate(features) if f > tol ) , reverse=True)
    # print("Have {} relevant features out of {} total".format(len(important), len(X_train[0])))
    # print("Important features are {}".format(important))


if __name__=="__main__":
    ptype()