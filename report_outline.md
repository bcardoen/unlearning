# Unlearners: CMPT 726 Project Report

- ​

## Introduction

***(should have 2-3 references here)***


- Field introduction
  - Machine learning is now more involved than ever in biomedical data
  - Medical imaging is such an example
  - Brain tumor and imaging
  - Previous work
- ML might seem like a jack of all trades, but it is not
  - We want to see if ML can be applied successfully to predict patient mortality from MRI data alone
  - It is difficult to (dis)prove a negative, but ~~mounting evidence~~ (exhaustive search) gives confidence in the case for its nonexistence
  - For that, we tried state-of-the-art ML methods to examine how well they perform
  - In our work we found evidence for the case that imaging alone is not sufficient to predict patient mortality

Related work : Random Forest on features on PET, predicting treatment response

@article{Desbordes2017,
author = {Desbordes, Paul and Ruan, Su and Modzelewski, Romain and Pineau, Pascal and Vauclin, S{\'{e}}bastien and Gouel, Pierrick and Michel, Pierre and {Di Fiore}, Fr{\'{e}}d{\'{e}}ric and Vera, Pierre and Gardin, Isabelle},
doi = {10.1371/journal.pone.0173208},
editor = {Ahmad, Aamir},
file = {:home/bcardoen/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Desbordes et al. - 2017 - Predictive value of initial FDG-PET features for treatment response and survival in esophageal cancer patients.pdf:pdf},
issn = {1932-6203},
journal = {PLOS ONE},
mendeley-groups = {Machine learning},
month = {mar},
number = {3},
pages = {e0173208},
publisher = {Public Library of Science},
title = {{Predictive value of initial FDG-PET features for treatment response and survival in esophageal cancer patients treated with chemo-radiation therapy using a random forest classifier}},
url = {http://dx.plos.org/10.1371/journal.pone.0173208},
volume = {12},
year = {2017}
}


## Approach

### Dataset

- About the dataset:
  - Where from?
  - Population of the sample
  - Name of the condition
- Dataset details:
  - Number of patients: 163 (without NaN in class label)
  - Description of each data point: Number of frames, dimension of each frame, number of channels, total number of dimensions
  - Y values: number of days survived after imaging (***Needs a bit more clarification***)
  Mean Y  Mean of Y 422.9631901840491 and Var 121528.61214196998

### Dimensionality reduction

- Computation with original data is infeasible
- We used CNN auto-encoder to reduce the number of dimensions down to 70K:
  - Quick description of auto-encoder architecture
  - Why CNN is good (***Need a reference here***)
  - ***Library? Parameters? Running time?***
- We further use SVD:
  - Dimensions after SVD: 163 (patients) by 163 (features)
  - sklearn.pca using 'full' solver, this applies SVD on the original data matrix
    normalization is applied on input matrix X (-mu / sd)
    Procedure :
    Input Matrix X : 163= N x 7e5 = P
    Normalize X : X = [[(x-mu)/sd) for x in row] for row in X]
    Construct correlation matrix  C = (X^T X) / (n-1) . In order for this to hold, X needs normalization, see previous point.
    C = (VSU^T USV^T)   / (n-1)   with X = USV^T (e.g. X is diagonalized, that is S is singular values matrix)
    \[
        C = \frac{VSU^T USV^T} {n-1} = V\frac{S^2} {n-1} V^T
    \]
    Principal components are then given by XV = USV^TV = US
    We select the k'th components that contribute the most to the variance (e.g. class) by reducing to U_kS_k, with K = N

    Reference :
    https://arxiv.org/abs/0909.4061
    @article{Halko2009,
    abstract = {Low-rank matrix approximations, such as the truncated singular value decomposition and the rank-revealing QR decomposition, play a central role in data analysis and scientific computing. This work surveys and extends recent research which demonstrates that randomization offers a powerful tool for performing low-rank matrix approximation. These techniques exploit modern computational architectures more fully than classical methods and open the possibility of dealing with truly massive data sets. This paper presents a modular framework for constructing randomized algorithms that compute partial matrix decompositions. These methods use random sampling to identify a subspace that captures most of the action of a matrix. The input matrix is then compressed---either explicitly or implicitly---to this subspace, and the reduced matrix is manipulated deterministically to obtain the desired low-rank factorization. In many cases, this approach beats its classical competitors in terms of accuracy, speed, and robustness. These claims are supported by extensive numerical experiments and a detailed error analysis.},
    archivePrefix = {arXiv},
    arxivId = {0909.4061},
    author = {Halko, Nathan and Martinsson, Per-Gunnar and Tropp, Joel A.},
    eprint = {0909.4061},
    file = {:home/bcardoen/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Halko, Martinsson, Tropp - 2009 - Finding structure with randomness Probabilistic algorithms for constructing approximate matrix decompo.pdf:pdf},
    month = {sep},
    title = {{Finding structure with randomness: Probabilistic algorithms for constructing approximate matrix decompositions}},
    url = {http://arxiv.org/abs/0909.4061},
    year = {2009}
    }

    Reason for SVD : numerical stability, computing SVD is slow (still ms in our case) but has higher numerical stability,
    with our time budget we could afford to apply full SVD.

    @article{Golub1965,
    abstract = {A numerically stable and fairly fast scheme is described to compute the unitary matrices U and V which transform a given matrix A into a diagonal form {\$}\backslashSigma = U{\^{}} * AV{\$}, thus exhibiting A's singular values on {\$}\backslashSigma {\$}'s diagonal. The scheme first transforms A to a bidiagonal matrix J, then diagonalizes J. The scheme described here is complicated but does not suffer from the computational difficulties which occasionally afflict some previously known methods. Some applications are mentioned, in particular the use of the pseudo-inverse {\$}A{\^{}}I = V\backslashSigma {\^{}}I U{\^{}}* {\$} to solve least squares problems in a way which dampens spurious oscillation and cancellation.},
    author = {Golub, G. and Kahan, W.},
    doi = {10.1137/0702016},
    issn = {0887-459X},
    journal = {Journal of the Society for Industrial and Applied Mathematics Series B Numerical Analysis},
    month = {jan},
    number = {2},
    pages = {205--224},
    publisher = {Society for Industrial and Applied Mathematics},
    title = {{Calculating the Singular Values and Pseudo-Inverse of a Matrix}},
    url = {http://epubs.siam.org/doi/10.1137/0702016},
    volume = {2},
    year = {1965}
    }







### Regression methods

- We used a variety of ML regression methods
- scikit-learn
- Data is split 4/5 into (Training/Validation) + Testing, so 130 / 33
- Then on 130 samples 5-fold is used to train the regressor and get the best param values
- Finally, we compare all regressors and score them using 5-kfold on the full 163 data set (with the omitted 33 samples)

#### Linear regression

- The two parameters
- ***lambda*** value calculation :( it doesn't expose that to the user Baraa :(

#### KNN regression

Why KNN, what do parameters mean?

From sk-Learn documentation:
Section 1.6 of http://scikit-learn.org/stable/modules/neighbors.html
<quote>
Neighbors-based methods are known as non-generalizing machine learning methods, since they simply “remember” all of its training data (possibly transformed into a fast indexing structure such as a Ball Tree or KD Tree.).
Despite its simplicity, nearest neighbors has been successful in a large number of classification and regression problems...
<unquote>    

Section 1.6.3 of Nearest Neighbours Regression: http://scikit-learn.org/stable/modules/neighbors.html#regression

<quote>
Neighbors-based regression can be used in cases where the data labels are continuous rather than discrete variables. The label assigned to a query point is computed based the mean of the labels of its nearest neighbors.

KNeighborsRegressor implements learning based on the  nearest neighbors of each query point, where  is an integer value specified by the user.

The basic nearest neighbors regression uses uniform weights: that is, each point in the local neighborhood contributes uniformly to the classification of a query point. Under some circumstances, it can be advantageous to weight points such that nearby points contribute more to the regression than faraway points. This can be accomplished through the weights keyword. The default value, weights = 'uniform', assigns equal weights to all points. weights = 'distance' assigns weights proportional to the inverse of the distance from the query point.
<unquote>

- ***Regression Model here?*** The algorithms used to learn can be any of the following based on some conditions quoted below:
Brute Force
K-D Tree [References:
“Multidimensional binary search trees used for associative searching”, Bentley, J.L., Communications of the ACM (1975)]
Ball Tree [References:
“Five balltree construction algorithms”, Omohundro, S.M., International Computer Science Institute Technical Report (1989)]

In our case, the regressor uses KD Tree with 30 leaf nodes and Minkowski as distance metric
[choice of algorithm is detailed in section: 1.6.4.4. Choice of Nearest Neighbors Algorithm of the same documentation page]

- ***K*** value choice: [5, 10, 20, ..., 80] <- old (when internal CV was performing on 3 folds. when number of cv folds was changed to 5, the best result according to Grid Search was (weight: uniform, n_neighbors: 70). Here we noticed some patterns on changing the k values:

Going from 5 to 25 to 80 we see a lowering MSE but a clustering pattern around 400 since as k/n (samples) goes to 1 we're overfitting on individual training samples
See figures in report_figures appended by k=5/25/50, we have search space coverage by employing grid search but these were interesting observations discovered in the process.

About Grid search: [http://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GridSearchCV.html#sklearn.model_selection.GridSearchCV]

parameters used: eastimator, param_grid, cv

Note:
<quote>
    estimator : estimator object - This is assumed to implement the scikit-learn estimator interface. Either estimator needs to provide a score function, or scoring must be passed.
<unquote>

About KNNRegressors default score function:
score(X, y, sample_weight=None)

Returns the coefficient of determination R^2 of the prediction.
The coefficient R^2 is defined as (1 - u/v), where u is the residual sum of squares ((y_true - y_pred) ** 2).sum() and v is the total sum of squares ((y_true - y_true.mean()) ** 2).sum(). The best possible score is 1.0 and it can be negative (because the model can be arbitrarily worse). A constant model that always predicts the expected value of y, disregarding the input features, would get a R^2 score of 0.0.


We see that going from 5 to 25 to 80 we see a lowering MSE but a clustering pattern around 400, since as k/n (samples) goes to 1 we're overfitting on individual training samples
See figures in report_figures appended by k=5/25/50, we have search space coverage by employing grid search but these were interesting observations discovered in the process.


#### Logistics  regression
GSearch obtained following parameters
liblinear solver (default), C=0.001 (inverse Regularization penalty, best out of for c in [0.0001, 0.001, 0.1, 0.2, 0.4, 0.8, 1.6, 3.2], l2 norm
http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html

### SVM not since AutoML uses this

#### Random forest regression

Best estimators = 81 found by Grid search, rest of parameters are MSE and autotuning
Params for best are {'alpha': 0.9, 'criterion': 'friedman_mse', 'init': None, 'learning_rate': 0.05, 'loss': 'ls', 'max_depth': 25, 'max_features': None, 'max_leaf_nodes': None, 'min_impurity_split': 1e-07, 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0.0, 'n_estimators': 100, 'presort': 'auto', 'random_state': 0, 'subsample': 1.0, 'verbose': 0, 'warm_start': False}


#### Gradient Boosting

- Gradient booster {ref}
- Constructs an ensemble of 100 trees of maximum depth 25 with least squares loss function. The depth allows us to capture interactions between features of orders up to 25. The huber
loss function (which combines L1 and L2 for robustness) was tried but did not yield improvements. The number of estimators and learning rate was chosen based on guidance in {refRidgeway2007} where
specifically it is argued that the learning rate should be kept small as GB can overfit contrary to common knowledge.

@article{Friedman2001,
author = {Friedman, Jerome H.},
doi = {10.1214/aos/1013203451},
file = {:home/bcardoen/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Friedman - 2001 - machine.pdf:pdf},
issn = {0090-5364},
journal = {The Annals of Statistics},
keywords = {Function estimation,boosting,decision trees,robust nonparametric regression},
month = {oct},
number = {5},
pages = {1189--1232},
publisher = {Institute of Mathematical Statistics},
title = {machine.},
url = {http://projecteuclid.org/euclid.aos/1013203451},
volume = {29},
year = {2001}
}


@article{Ridgeway2007,
abstract = {Boosting takes on various forms with different programs using different loss functions, different base models, and different optimization schemes. The gbm package takes the approach described in [2] and [3]. Some of the terminology differs, mostly due to an effort to cast boosting terms into more standard sta-tistical terminology (e.g. deviance). In addition, the gbm package implements boosting for models commonly used in statistics but not commonly associated with boosting. The Cox proportional hazard model, for example, is an incred-ibly useful model and the boosting framework applies quite readily with only slight modification [5]. Also some algorithms implemented in the gbm package differ from the standard implementation. The AdaBoost algorithm [1] has a particular loss function and a particular optimization algorithm associated with it. The gbm implementation of AdaBoost adopts AdaBoost's exponential loss function (its bound on misclassification rate) but uses Friedman's gradient de-scent algorithm rather than the original one proposed. So the main purposes of this document is to spell out in detail what the gbm package implements. 1 Gradient boosting This section essentially presents the derivation of boosting described in [2]. The gbm package also adopts the stochastic gradient boosting strategy, a small but important tweak on the basic algorithm, described in [3].},
author = {Ridgeway, Greg},
file = {:home/bcardoen/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Ridgeway - 2007 - Generalized Boosted Models A guide to the gbm package.pdf:pdf},
mendeley-groups = {Machine learning},
title = {{Generalized Boosted Models: A guide to the gbm package}},
url = {http://www.saedsayad.com/docs/gbm2.pdf},
year = {2007}
}



#### AutoML

Note : K fold not used, that is, AutoML is given training (130) set to do internal preprocessing and data handling, then scored on testing (e.g. black box)
And I'm not running it 2x5x1 hours

- Ensemble
- ***List*** of models (e.g. paper says all, so at time of publishing this was all 14, but best to use just 'all')
 The below is from the paper

AdaBoost (AB) 4 1 (-) 3 (-)
Bernoulli na¨ıve Bayes 2 1 (-) 1 (-)
decision tree (DT) 4 1 (-) 3 (-)
extreml. rand. trees 5 2 (-) 3 (-)
Gaussian na¨ıve Bayes - - -
gradient boosting (GB) 6 - 6 (-)
kNN 3 2 (-) 1 (-)
LDA 4 1 (-) 3 (1)
linear SVM 4 2 (-) 2 (-)
kernel SVM 7 2 (-) 5 (2)
multinomial na¨ıve Bayes 2 1 (-) 1 (-)
passive aggressive 3 1 (-) 2 (-)
QDA 2 - 2 (-)
random forest (RF) 5 2 (-) 3 (-)
Linear Class. (SGD) 1

- ***Default parameters***? --> parameters allow exclusion of regressors (not done obviously) and exclusion of preprocessing methods (done since we argue SVD is the best)
- Runtime:
  - 1-24 hours, no improvement +-5%, flatline convergence
  Uses Bayesian Optimization, SMAC (for hyperparameters) on each classifier/regressor, then forms an ensemble using the results from that optimization to find an optimal match.
  Preprocessing is also automatically offered, but we disabled (since we PCA'ed ourselves).

Why this ? Cover bases, better than state of the art (eg. autoweka)




@article{Feurer,
abstract = {The success of machine learning in a broad range of applications has led to an ever-growing demand for machine learning systems that can be used off the shelf by non-experts. To be effective in practice, such systems need to automatically choose a good algorithm and feature preprocessing steps for a new dataset at hand, and also set their respective hyperparameters. Recent work has started to tackle this automated machine learning (AutoML) problem with the help of efficient Bayesian optimization methods. Building on this, we introduce a robust new AutoML system based on scikit-learn (using 15 classifiers, 14 feature preprocessing methods, and 4 data preprocessing methods, giving rise to a structured hypothesis space with 110 hyperparameters). This system, which we dub AUTO-SKLEARN, improves on existing AutoML methods by automatically taking into account past performance on similar datasets, and by constructing ensembles from the models evaluated during the optimization. Our system won the first phase of the ongoing ChaLearn AutoML challenge, and our comprehensive analysis on over 100 diverse datasets shows that it substantially outperforms the previous state of the art in AutoML. We also demonstrate the performance gains due to each of our contributions and derive insights into the effectiveness of the individual components of AUTO-SKLEARN.},
author = {Feurer, Matthias and Klein, Aaron and Eggensperger, Katharina and Springenberg, Jost Tobias and Blum, Manuel and Hutter, Frank},
file = {:home/bcardoen/.local/share/data/Mendeley Ltd./Mendeley Desktop/Downloaded/Feurer et al. - Unknown - Efficient and Robust Automated Machine Learning.pdf:pdf},
title = {{Efficient and Robust Automated Machine Learning}},
url = {http://papers.nips.cc/paper/5872-efficient-and-robust-automated-machine-learning.pdf}
}



### Correlation measure

- Pearson R
   r12 = [(Yi_1 - \muY_1)*(Yi_2 - \muY_2)] / \sqrt{ [(Yi_1 - \muY_1)2 * (Yi_2 - \mu_Y_2)2]}
  range -1/0/1, where 0 is non linear correlation, -1 is negative, 1 is linear correlation
  Use it because MSE doesn't take into account correlation



## Experiments and results

### Dimensionality reduction

- CNN:
  - ***Loss?*** Figure
- ~~SVD:~~
  - ~~Dimensions after SVD: 163 by 163~~
  - ~~***Any parameters used here? Library?***~~

### Regression methods

- We are running with SVD compressed data

#### Linear regression

- With SVD ***Figure***: Model vs. real data

#### KNN regression

- With SVD ***Figure***: Model vs. real data

#### ~~Random forest regression~~

- ~~With SVD ***Figure***: Model vs. real data~~

#### Gradient Boosting

- With SVD ***Figure***: Model vs. real data

#### AutoML

- With SVD ***Figure***: Model vs. real data

This is the ensemble used by AutoML

[(0.360000, SimpleRegressionPipeline({'imputation:strategy': 'median', 'one_hot_encoding:use_minimum_fraction': 'True', 'preprocessor:__choice__': 'no_preprocessing', 'regressor:__choice__': 'gradient_boosting', 'rescaling:__choice__': 'normalize', 'one_hot_encoding:minimum_fraction': 0.010000000000000004, 'regressor:gradient_boosting:learning_rate': 0.10000000000000002, 'regressor:gradient_boosting:loss': 'huber', 'regressor:gradient_boosting:max_depth': 3, 'regressor:gradient_boosting:max_features': 1.0, 'regressor:gradient_boosting:max_leaf_nodes': 'None', 'regressor:gradient_boosting:min_samples_leaf': 1, 'regressor:gradient_boosting:min_samples_split': 7, 'regressor:gradient_boosting:min_weight_fraction_leaf': 0.0, 'regressor:gradient_boosting:n_estimators': 50, 'regressor:gradient_boosting:subsample': 0.49458861836996104, 'regressor:gradient_boosting:alpha': 0.9},
dataset_properties={
  'task': 4,
  'sparse': False,
  'multilabel': False,
  'multiclass': False,
  'target_type': 'regression',
  'signed': False})),
(0.340000, SimpleRegressionPipeline({'imputation:strategy': 'median', 'one_hot_encoding:use_minimum_fraction': 'False', 'preprocessor:__choice__': 'no_preprocessing', 'regressor:__choice__': 'gradient_boosting', 'rescaling:__choice__': 'normalize', 'regressor:gradient_boosting:learning_rate': 0.10000000000000002, 'regressor:gradient_boosting:loss': 'huber', 'regressor:gradient_boosting:max_depth': 3, 'regressor:gradient_boosting:max_features': 1.0, 'regressor:gradient_boosting:max_leaf_nodes': 'None', 'regressor:gradient_boosting:min_samples_leaf': 1, 'regressor:gradient_boosting:min_samples_split': 7, 'regressor:gradient_boosting:min_weight_fraction_leaf': 0.0, 'regressor:gradient_boosting:n_estimators': 100, 'regressor:gradient_boosting:subsample': 0.5264818842520748, 'regressor:gradient_boosting:alpha': 0.9},
dataset_properties={
  'task': 4,
  'sparse': False,
  'multilabel': False,
  'multiclass': False,
  'target_type': 'regression',
  'signed': False})),
(0.300000, SimpleRegressionPipeline({'imputation:strategy': 'median', 'one_hot_encoding:use_minimum_fraction': 'False', 'preprocessor:__choice__': 'no_preprocessing', 'regressor:__choice__': 'gradient_boosting', 'rescaling:__choice__': 'normalize', 'regressor:gradient_boosting:learning_rate': 0.10000000000000002, 'regressor:gradient_boosting:loss': 'huber', 'regressor:gradient_boosting:max_depth': 3, 'regressor:gradient_boosting:max_features': 1.0, 'regressor:gradient_boosting:max_leaf_nodes': 'None', 'regressor:gradient_boosting:min_samples_leaf': 1, 'regressor:gradient_boosting:min_samples_split': 7, 'regressor:gradient_boosting:min_weight_fraction_leaf': 0.0, 'regressor:gradient_boosting:n_estimators': 50, 'regressor:gradient_boosting:subsample': 0.5264818842520748, 'regressor:gradient_boosting:alpha': 0.9},
dataset_properties={
  'task': 4,
  'sparse': False,
  'multilabel': False,
  'multiclass': False,
  'target_type': 'regression',
  'signed': False})),
]

### Correlation measure

- ***$R$******value*** for each method to be put in a table
Executing pipeline

Results for  <class 'booster.GBRTrainer'> :: AVG MSE k=5 256059.8549635435 avg R  -0.0892946815826213

Results for  <class 'rgrModel.LRTrainer'> :: AVG MSE k=5 313047.87209351215 avg R  0.07582161511002666

Results for  <class 'KNN.KNNTrainer'> :: AVG MSE k=5 125048.09463218926 avg R  -0.11339871239402413

Results for Random Forest   AVG MSE : 142876.4466384813 avg R k=5 -0.12434131698874036

Logistic regression AVG MSE k=5 198153.70397727273 avg R k=5 0.11512022726995053

Best trainer is <KNN.KNNTrainer object at 0x7f27762c8b70> with info 42 and mean loss 125048.09463218926

AutoML : R 0.091467926337934713, MSE 153341.637248



## Conclusion

- With SVD ***Figure***: Model vs. real data
- To cover our basis: with(out) SVD
- Convergence of CNN/AutoML
- State of the art methods
- ***MSE is not enough*** $r^2$: (other paper)

## Work Distribution



## References
