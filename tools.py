from functools import reduce

def minmax(values):
    '''
    Return the minimum and maximum of an iterable.
    :param values:
    :return:
    '''
    return reduce(lambda ac, i: (i if i < ac[0] else ac[0], i if i > ac[1] else ac[1]), values, (float('inf'), -float('inf')))


def mleGaussian(values):
    '''
    :param values: List of numerical typed values
    :return: Estimated mean, variance and normalized set of values
    '''
    N = len(values)
    mlemean = sum(values) / N
    offset = ((x - mlemean) for x in values)
    var = sum( o ** 2 for o in offset) / N
    assert(var != 0)
    offset = ((x - mlemean) for x in values) # Tee won't work here
    normalized = ( o / var for o in offset)
    return mlemean, var, normalized
