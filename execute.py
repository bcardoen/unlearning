from pipeline import Pipeline, Trainer, Loss, Model
from datafilter import loadData
import numpy as np
from pipeline import Pipeline
from booster import GBRTrainer
from rgrModel import LRTrainer
from logreg import LogRTrainer
from KNN import KNNTrainer
from forest import FTrainer

if __name__ == "__main__":
    XTrain, yTrain, XTest, yTest, XFull, YFull = loadData(pca=True)
    trainers = [GBRTrainer(k=5), LRTrainer(k=5), KNNTrainer(k=5), FTrainer(k=5), LogRTrainer(k=5)] # Add your trainers here
    pipeline = Pipeline(k=5, trainers=trainers, X=XFull, y=YFull)
    pipeline()
