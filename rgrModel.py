import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from pipeline import MSELoss, Model, Trainer
import os
from tools import mleGaussian
from datafilter import prepareData, loadData
import pandas as pd
from sklearn.model_selection import cross_val_predict


class LRModel(Model):
      def __init__(self, X_train, y_train, parameter_set):
            y_train = y_train.ravel()
            super().__init__(X_train, y_train, parameter_set)

      def _train(self):
            self._est = linear_model.LinearRegression().fit(self.X_train,self.y_train)
            # features = self._est.feature_importances_
            self.info = "<Bdour please update description here >"

      def __call__(self, X_test):
            y_predict = self._est.predict(X_test)
            return y_predict


class LRTrainer(Trainer):
      def __init__(self, k, loss_object=MSELoss()):
            super().__init__(k, loss_object=loss_object)
            self.model_type = LRModel
            self.parameter_space[0] = {'n_neighbors': 5, 'weights': 'distance'}


if __name__ == "__main__":
      # Create linear regression object
      # Use SVD
      xtrain, ytrain, xtest, ytest, XFull, YFull = loadData(pca=True)
      # Don't use SVD
      # xtrain, ytrain, xtest, ytest, XFull, YFull = loadData(pca=False)
      regr = linear_model.LinearRegression()

      # Train the model using the training sets
      regr.fit(xtrain, ytrain)

      # Make predictions using the testing set
      y_pred = regr.predict(xtest)

      print("shape of predicted")
      print(np.shape(y_pred))

      # The coefficients
      print('Coefficients: \n', regr.coef_)
      # The mean squared error
      print("Mean squared error: %.2f"
            % mean_squared_error(ytest, y_pred))
      # Explained variance score: 1 is perfect prediction
      print('Variance score: %.2f' % r2_score(ytest, y_pred))

      fig, ax = plt.subplots()
      ax.scatter(ytest, y_pred, edgecolors=(0, 0, 0))
      ax.plot([ytest.min(), ytest.max()], [ytest.min(), ytest.max()], 'k--', lw=4)
      ax.set_xlabel('Measured')
      ax.set_ylabel('Predicted')
      plt.show()