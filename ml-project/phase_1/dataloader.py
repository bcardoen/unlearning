from __future__ import print_function
import nibabel as nb
import glob as glob
import numpy as np
import skimage as skimage
import matplotlib.pyplot as plt
from skimage import transform
import pickle as pickle
import sys as sys
from pandas import read_csv

def loadData(data_dir, saveDump=False, loadFromDump=False, loadStandardized=False,
             out_shape=(120, 120), num_slices=155, 
             num_sequences=4, num_patients=210, verbose=False, loadAnnotations=False, 
             annotationFilepath=None, loadSeg=False):
    
    print('[WARNING] The loader DOES NOT STANDARDIZE DATA, NEITHER IT WRITES STANDARDIZED DATA TO DISK', file=sys.stderr)
    print('It can only load standardized data from disk..', file=sys.stderr)
    if loadFromDump == False:
        print('Loading images...')
        patID = 0
        images = np.empty((num_patients, num_sequences, out_shape[0], out_shape[1], num_slices))
        labels = np.empty((num_patients, 1))
        
        if loadSeg == True:
            seg_masks = np.empty((num_patients, out_shape[0], out_shape[1], num_slices))
        
        csv_flag = 0
        
        if loadAnnotations == True:
            print('Loading annotations as well..')
            if annotationFilepath != None:
                csv_file = read_csv(annotationFilepath)
                csv_flag = 1
                print('Opened CSV file successfully.')
            else:
                print('loadAnnotations is True but no CSV filepath provided!')
                sys.exit()
                
        print('Starting to load images..')
        for patients in glob.glob(data_dir + '/*'):
            i = 0
            if verbose == True:
                print('Currently on patient {}'.format(patID+1))
                
            if csv_flag == 1:
                patName = patients.split('/')[-1]
                try:
                    labels[patID] = csv_file[csv_file['Brats17ID'] == patName]['Survival'].tolist()[0]
                    print('Added survival data..')
                except IndexError:
                    labels[patID] = None
                
            for imagefile in glob.glob(patients + '/*'):
                if 'seg' in imagefile:
                    if loadSeg == True:
                        print('Loading segmentation for this patient..')
                        img_obj = nb.load(imagefile)
                        pix_data = img_obj.get_data()

                        ims_resized = np.empty((out_shape[0], out_shape[1], num_slices))
                        for ims in range(num_slices):
                            ims_resized[:, :, ims] = transform.resize(pix_data[:,:,ims], output_shape=out_shape)

                        seg_masks[patID,:,:,:] = ims_resized
                    else:
                        continue
                else:
#                     print('Loading the file {}'.format(imagefile.split('/')[-1]))
                    img_obj = nb.load(imagefile)
                    pix_data = img_obj.get_data()

                    ims_resized = np.empty((out_shape[0], out_shape[1], num_slices))
                    for ims in range(num_slices):
                        ims_resized[:, :, ims] = transform.resize(pix_data[:,:,ims], output_shape=out_shape)

                    images[patID, i,:,:,:] = ims_resized
                    i += 1
            patID += 1
        if saveDump == True:
            print('Dumping data to disk...')
            np.save(open('./training_data_orig.npy', 'wb'), images)
    else:
        if loadStandardized == False:
            images = np.load(open('./training_data_orig.npy', 'rb'))
        else:
            images = np.load(open('./training_data_std.npy', 'rb'))
       
    if csv_flag == 1 and loadSeg == True:
        return images, labels, seg_masks
    elif csv_flag == 0 and loadSeg == True:
        return images, seg_masks
    elif csv_flag == 0 and loadSeg == False:
        return images

def standardize(images, saveDump=False):
    print('Calculating mean value..')
    mn = []
    for i in range(4):
        mn.append(np.mean(images[:, i, :, :, :]))
    
    print('Calculating standard deviation..')
    std = []
    for i in range(4):
        std.append(np.std(images[:, i, :, :, :]))
        
    
    print('Starting standardization process..')
    
    for i in range(4):
        images[:, i, :, :, :] = ((images[:, i, :, :, :] - mn[i]) / float(std[i]))
        
    print('Data standardized!')
    
    if saveDump == True:
        print('Dumping standardized data to disk..')
        np.save(open('./training_data_std.npy', 'wb'), images)
    print('Done!')
    
    return images


def visualizeReconstruction(orig, pred):
    n = 400
	# (32550, 120, 120, 4)
    for i in range(0, n, 10):
        # display original
        print('='*100)
        f, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize=(10,10))
        ax1.set_title('T2Flair')
        ax1.imshow(orig[i,:,:,0], cmap='gray')
        ax2.set_title('T1')
        ax2.imshow(orig[i,:,:,1], cmap='gray')
        ax3.set_title('T1c')
        ax3.imshow(orig[i,:,:,2], cmap='gray')
        ax4.set_title('T2')
        ax4.imshow(orig[i,:,:,3], cmap='gray')
        ax1.axis('off')
        ax2.axis('off')
        ax3.axis('off')
        ax4.axis('off')
        f.tight_layout()
        f.subplots_adjust(top=1.65)
        plt.suptitle('Original Images')
        plt.show()

        # display reconstruction
        f, (ax1, ax2, ax3, ax4) = plt.subplots(1, 4, figsize=(10,10))
        ax1.set_title('T2Flair')
        ax1.imshow(pred[i,:,:,0], cmap='gray')
        ax2.set_title('T1')
        ax2.imshow(pred[i,:,:,1], cmap='gray')
        ax3.set_title('T1c')
        ax3.imshow(pred[i,:,:,2], cmap='gray')
        ax4.set_title('T2')
        ax4.imshow(pred[i,:,:,3], cmap='gray')
        ax1.axis('off')
        ax2.axis('off')
        ax3.axis('off')
        ax4.axis('off')
        f.tight_layout()
        f.subplots_adjust(top=1.65)
        plt.suptitle('Decoded Images')
        plt.show()
        print('='*100)
        print('\n')
