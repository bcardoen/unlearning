import dataloader as dl
import numpy as np
import matplotlib.pyplot as plt
from keras.model import Model, load_model

# In[2]:


data_dir = '/home/asa224/Desktop/students_less_asa224/BRATS2017/MICCAI_BraTS17_Data_Training/HGG/'
images = dl.loadData(data_dir, loadFromDump=False, loadStandardized=False, saveDump=False, verbose=1)


# In[3]:


images = dl.standardize(images)

images_rs = np.swapaxes(images, 1, 2)
images_rs = np.swapaxes(images_rs, 2, 3)
images_rs = np.swapaxes(images_rs, 3, 4)

model = load_model('../models/checkpoints/AE_3D_v-2-1_epoch-49-valloss-0.11.hdf5')

model.summary()

dataset = 
