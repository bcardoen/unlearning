import pandas as pd
from sklearn.decomposition import PCA, IncrementalPCA
import numpy as np
import os
from tools import mleGaussian

dbpath = os.path.join("ml-project", "database")
names = ['xtrain', 'ytrain', 'xtest', 'ytest']
full = ['xfull', 'yfull']

def loadMatrix(filename):
    return np.load(filename, allow_pickle=True)


def loadData(pca=True):
    matrices = []
    for name in names:
        fname = name + ("_reduced" if pca else "") + ".npy"
        matrices.append(loadMatrix(os.path.join(dbpath, fname)))
    for n in full:
        fname = n + ("_reduced" if pca else "") + ".npy"
        matrices.append(loadMatrix(os.path.join(dbpath, fname)))
    return matrices


def prepareData(X = os.path.join(dbpath, "dataset_master.csv"), Y = os.path.join(dbpath,"survival_master.csv"), split = 4/5.0, ApplyPCA=False, writeOutput=False, seed = 0, normalizeY=False):
    '''
    Reads in Anmol's data, splits into 4 matrices, XTrain, Ytrain, XTest, YTest according to split ratio
    :param split:
    :param ApplyPCA: Reduce dimensions with SVD
    :param writeOutput: Write all data to npy
    :param seed: Seed for shuffling matrices.
    :return: xtrain, ytrain, xtest, ytest, xfull, yfull or reduced versions if pca = True
    '''
    names = ['xtrain', 'ytrain', 'xtest', 'ytest']
    assert(split > 0 and split < 1)
    XD = pd.DataFrame.from_csv(X)
    YD = pd.DataFrame.from_csv(Y)
    dropNan = [ index for index, r in YD.iterrows() if np.isnan(r[0])]

    print("Y has {} Nan values, removing...".format(len(dropNan)))

    XD.drop(XD.index[dropNan], inplace=True)
    YD.drop(YD.index[dropNan], inplace=True)
    print("X has now {} records and Y has {} records".format(len(XD), len(YD)))
    recordssplit = int(len(XD) * split)
    print("Splitting into Training and Testing data with {} as split".format(recordssplit))

    XDM, YDM = XD.as_matrix(), YD.as_matrix()
    np.random.seed(seed)
    np.random.shuffle(XDM)
    np.random.seed(seed)
    np.random.shuffle(YDM)
    fullmatrices = [ XDM[:recordssplit], YDM[:recordssplit], XDM[recordssplit:], YDM[recordssplit:]]

    pcamatrices = []
    XDMP, YDMP = XDM.copy(), YDM.copy()
    if ApplyPCA:
        pca = PCA(svd_solver='full')
        XDMP = pca.fit_transform(XDM, y=None) # Don't use Y, it's ignored in the iface.

        if normalizeY:
            r = mleGaussian(YDMP[:,0])
            print("Mean of Y {} and Var {}".format(r[0], r[1]))
            YDMP[:,0] = list(r[2])
            print("")

        pcamatrices = [XDMP[:recordssplit], YDMP[:recordssplit], XDMP[recordssplit:], YDMP[recordssplit:]]

    if writeOutput:
        for name, fmatrix in zip(full , [XDM, YDM]):
            np.save(os.path.join(dbpath, name + ".npy"), fmatrix, allow_pickle=True)
        for name, fmatrix in zip(names, fullmatrices):
            np.save(os.path.join(dbpath, name + ".npy"), fmatrix, allow_pickle=True)
        if ApplyPCA:
            for name, pmatrix in zip(names, pcamatrices):
                np.save(os.path.join(dbpath, name + "_reduced" + ".npy"), pmatrix, allow_pickle=True)
            for name, fmatrix in zip(full, [XDMP, YDMP]):
                np.save(os.path.join(dbpath, name + "_reduced" + ".npy"), fmatrix, allow_pickle=True)

    return pcamatrices, fullmatrices, [XDM, YDM], [XDMP, YDMP]


if __name__ == "__main__":
    prepareData(ApplyPCA=True, writeOutput=True)
    Xtr = loadMatrix(os.path.join(dbpath, "xtrain.npy"))
